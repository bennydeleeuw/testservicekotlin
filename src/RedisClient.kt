package com.example

import redis.clients.jedis.Jedis


private val client = RedisClient()

fun getRedisClient() : RedisClient {
    return client;
}
class RedisClient {

    val jedis = Jedis("localhost")

    fun put(key:String, value:String) {
        jedis.set(key, value)
    }

}