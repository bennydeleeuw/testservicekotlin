package com.example

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.testRouter() {

    val client = getRedisClient()

    get("/xxx") {
        client.put("x", "y")
        call.respond("Hi!s")
    }

    get("/yyy") {
        call.respond("Dit is een test!")
    }
}