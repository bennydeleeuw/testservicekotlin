package com.example

import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry

private val registry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

fun getRegistry(): PrometheusMeterRegistry {


    return registry
}