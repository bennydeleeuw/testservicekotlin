package com.example

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*


fun Routing.metrics() {

    val registry = getRegistry()

    get("/metrics") {
        call.respondText {
            registry.scrape()
        }
    }
//
    trace {
        val path = it.call.request.uri
        val responeCode = it.call.response.status()

    }
}
